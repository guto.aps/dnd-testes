import "@atlaskit/css-reset";
import Board from "./components/Board";
import { Container } from "./style";
import NavBarProjects from "./components/NavBarProjects";

function App() {
  return (
    <>
      <NavBarProjects />
      <Container>
        <Board />
      </Container>
    </>
  );
}

export default App;
