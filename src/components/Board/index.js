import { useState, useEffect } from "react";
import initialData from "../../initial-data";
import Column from "../Column";
import "@atlaskit/css-reset";
import { DragDropContext } from "react-beautiful-dnd";
import { Container } from "./style";
import axios from "axios";

const Board = () => {
  const [data, setData] = useState(initialData);
  /* const [token, setToken] = useState(
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Imd1dG8uYXBzQGdtYWlsLmNvbSIsImlhdCI6MTYxODM3NjcwNSwiZXhwIjoxNjE4MzgwMzA1LCJzdWIiOiIxIn0.UD6i-5nf72lG_x4kNl7Kf1R39SXcFZGi0y-ac-qDAqI"
  );

  useEffect(() => {
    axios
      .get(`https://json-server-demo-guto.herokuapp.com/tasks/`)
      .then((resp) => {
        console.log(resp);
      });
  }, []); */

  const onDragEnd = (result) => {
    const { destination, source, draggableId } = result;

    if (!destination) {
      return;
    }

    if (
      destination.droppableId === source.droppableId &&
      destination.index === source.index
    ) {
      return;
    }

    const start = data.columns[source.droppableId];
    const finish = data.columns[destination.droppableId];

    console.log(start, "start");
    console.log(finish, "finish");

    if (start === finish) {
      const newTaskIds = Array.from(start.taskIds);
      newTaskIds.splice(source.index, 1);
      newTaskIds.splice(destination.index, 0, draggableId);

      const newColumn = {
        ...start,
        taskIds: newTaskIds,
      };

      const newState = {
        ...data,
        columns: { ...data.columns, [newColumn.id]: newColumn },
      };

      setData(newState);
      return;
    }

    const startTaskIds = Array.from(start.taskIds);
    startTaskIds.splice(source.index, 1);
    const newStart = {
      ...start,
      taskIds: startTaskIds,
    };

    const finishTaskIds = Array.from(finish.taskIds);
    finishTaskIds.splice(destination.index, 0, draggableId);
    const newFinish = {
      ...finish,
      taskIds: finishTaskIds,
    };

    const newState = {
      ...data,
      columns: {
        ...data.columns,
        [newStart.id]: newStart,
        [newFinish.id]: newFinish,
      },
    };
    setData(newState);
    console.log(newState);
  };

  return (
    <DragDropContext onDragEnd={onDragEnd}>
      <Container>
        {data.columnsOrder.map((columnId) => {
          const column = data.columns[columnId];
          const tasks = column.taskIds.map((taskId) =>
            data.tasks.find((task) => task.id === taskId)
          );
          return <Column key={column.id} column={column} tasks={tasks} />;
        })}
      </Container>
    </DragDropContext>
  );
};

export default Board;
